/* @brief  EDU-CIAA NXP GPIO driver
 * @author Levrino Micaela
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

/*==================[inclusions]=============================================*/
#include "Tcrt5000.h"
#include "gpio.h"
#include "delay.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

gpio_t dout_local;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool Tcrt5000Init(gpio_t dout){

	GPIOInit(dout, GPIO_INPUT);
	dout_local=dout;
	return 1;
}

bool Tcrt5000State(void){

	gpio_t state;

	if(GPIORead(dout_local)==true){
		state=1;
	}
	if(GPIORead(dout_local)==false){
		state=0;
	}

	return state;
}

bool Tcrt5000Deinit(gpio_t dout){
	GPIODeinit();
	return 1;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
