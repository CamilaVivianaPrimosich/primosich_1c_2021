/* @brief  EDU-CIAA NXP GPIO driver
 * @author Levrino Micaela
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

gpio_t echo_local;
gpio_t trigger_local;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger){

	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);

	echo_local=echo;
	trigger_local=trigger;

}

int16_t HcSr04ReadDistanceCentimeters(void){

	uint16_t count=0;
	uint16_t distance_cm;

	GPIOOn(trigger_local);
	DelayUs(10);
	GPIOOff(trigger_local);

	while(GPIORead(echo_local)==false){
	}
	while(GPIORead(echo_local)==true){
		DelayUs(1);
		count++; /* Cuento tiempos de a 1 useg */
	}

	distance_cm=count/58;
	return distance_cm;
}

int16_t HcSr04ReadDistanceInches(void){

	uint16_t count=0;
	uint16_t distance_in;

	GPIOOn(trigger_local);
	DelayUs(10);
	GPIOOff(trigger_local);

	while(GPIORead(echo_local)==false){
	}
	while(GPIORead(echo_local)==true){
		DelayUs(1);
		count++; /* Cuento tiempos de a 1 useg */
	}

	distance_in=count/148;
	return distance_in;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){
	GPIODeinit();
}
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
