/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Camila Primosich
 *
 */


/*Convierta una señal digital de un ECG (provista por la cátedra)
 * en una señal analógica y visualice esta señal utilizando el osciloscopio que acaba de implementar.
 * Se sugiere utilizar el potenciómetro para conectar la salida del DAC a la entrada CH1 del AD.
 * */


/*==================[inclusions]=============================================*/
#include "Proyecto_4_ECG.h"       /* <= own header */

#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

#define BAUDIO 115200  /*Transmitir los datos por la UART en formato ASCII a una velocidad de
						transmisión suficiente para realizar conversiones a la frecuencia requerida. */
#define BUFFER_SIZE 231
const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

bool bandera_timer= false;
uint16_t analog_value;
uint8_t i=0;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void FuncionTimer(){
	/*Disparar la conversión AD (es decir que comience la conversion) a través de una interrupción periódica de timer.*/
	AnalogStartConvertion();
}

void FuncionAnalog(){
	AnalogInputRead(CH1,&analog_value);
	UartSendString(SERIAL_PORT_PC, UartItoa(analog_value, 10));
	UartSendString(SERIAL_PORT_PC,"\r");
	/*Se deben enviar los datos convertidos separados por el caracter de fin de línea
	* ("11,22,33\r" para el caso de 3 canales con los datos 11, 22 y 33 para
	* el canal 1, 2 y 3 respectivamente)*/
}

void FuncionUart(){

}


void FuncionTimerB(){

	AnalogOutputWrite(ecg[i]);
	i++;
	if(i==231)
		i=0;
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();

	timer_config my_timer={TIMER_A,2, FuncionTimer}; /* frec muestreo f=500 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);


	timer_config my_timerB={TIMER_B,5, FuncionTimerB}; /* frec muestreo f=200 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&my_timerB);
	TimerStart(my_timerB.timer);

	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);
	AnalogOutputInit();

	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);

    while(1){

	}
    

}

/*==================[end of file]============================================*/

