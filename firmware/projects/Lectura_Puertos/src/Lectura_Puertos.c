/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Camila Primosich
 *
 *
 */

/*Ejercicio: Para corroborar la señal de la placa Arduino desarrolle un firmware que prenda el LED_2 de la EDU-CIAA
 * a partir de la lectura continua del pin TCOL_1. Utilice los drivers de gpio.h y led.h provistos por la cátedra.*/

/*==================[inclusions]=============================================*/
#include "Lectura_Puertos.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "bool.h"
/*==================[macros and definitions]=================================*/

bool variable= false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	LedsInit();

	GPIOInit(GPIO_T_COL0, GPIO_INPUT);

    while(1){

    	variable=GPIORead(GPIO_T_COL0);

    	if(variable == true){
    		LedOn(LED_2);
    		variable=false;
    	}
    	else
    		LedsOffAll();


    }

}
    

/*==================[end of file]============================================*/

