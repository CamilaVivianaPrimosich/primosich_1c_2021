/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/modulo_LCD.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	uint16_t num= 996;
	gpio_t pin[7]= { GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5 };
	SystemClockInit();
	ITSE0803Init(pin);


    while(1){
    	ITSE0803DisplayValue(num);
	}
    
	return 0;
}

/*==================[end of file]============================================*/

