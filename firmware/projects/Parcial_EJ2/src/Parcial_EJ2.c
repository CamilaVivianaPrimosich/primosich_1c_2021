/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 9/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 *
 *
 * Se desea realizar un control de fase digital para controlar la velocidad de un motor universal.
Se necesita digitalizar la senoidal de la red con 1 ms de resolución y detectar el cruce por cero,
 y luego de R0 milisegundos, disparar un triac con un pulso de 1ms.
 *
 *
 *
 * @author Camila Primosich
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial_EJ2.h"

#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/

#define BAUDIO 115200  /*Transmitir los datos por la UART en formato ASCII a una velocidad de
						transmisión suficiente para realizar conversiones a la frecuencia requerida. */
#define BUFFER_SIZE 231
#define R0_MAX 20
#define R0_MIN 0
#define PASO_MS 5
#define PASO 5
#define VAL 200 /*por defecto se eligio un vector con 200 muestras pero podrian ser mas o menos, por ello se lo define con define para que se pueda cambiar rapido el valor */
#define DIG 1023

float VAL_MEDIO_VOLT=1.65;
float r0=25; /*por defecto r0 es de 10 ms */


uint16_t datos[VAL]; /* variable para almacenar datos leidos del conversor */
uint8_t cont=0;    /*variable utilizada para saber el numero de muestras */
bool datoscargados=0; /*variable utilizada para saber si terminaron las 100 muestras*/
bool encendido=0;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void FuncionTimerADC(){
	/*Disparar la conversión AD (es decir que comience la conversion) a través de una interrupción periódica de timer.*/
	AnalogStartConvertion();
}

void FuncionAnalog(){
	AnalogInputRead(CH1,&analog_value); /*leemos un nuevo dato*/
	bandera_adc=true;/*Cambio de bandera de conversión AD */

		if(cont <= VAL){
			datoscargados=false;
			AnalogInputRead(CH1,&datos[cont]);
			cont++;
		}
		else
		{
		  cont=0;
		  datoscargados=true; /* uso esta variable para saber si tengo todos los datos cargados VAL en el vector */
		}
}


void DetectarCrucePorCero( uint16_t val[100] ){

	uint16_t maxx=val[0];
	uint8_t i=0;

	if(datoscargados==true){
				VAL_MEDIO_VOLT

				  for(i=0;i<=VAL;i++){

					  /*deberia de haber recorrido el vector y despues hacer el calculo del valor medio, pero no me alcanzo el tiempo */
					}
				}

				valor_medio=val_medio*VAL_MEDIO_VOLT; /*regla de 3 para encontrar el valor en voltaje*/
				valor_medio=valor_medio/DIG;


}


void FuncionTimerDAC(){
		bandera_dac=true;  /*Bandera de conversión DA */
}


void FuncionUart(){
	uint8_t dato;

	UartReadByte(SERIAL_PORT_PC,&dato);

	if (dato == 'B'){ /*leo el valor que tengo en el puerto, si es B MAYUSCULA, decrementa en 5 el valor de r0*/

			if (r0 > R0_MIN) { /*Si el valor de r0  es mayor que el valor minimo establecido, entra al if*/

					r0 = r0 - PASO_MS;
			}

			else
					r0 = r0;
	}

	if (dato == 'S'){ /*leo el valor que tengo en el puerto, si es S MAYUSCULA entra AL if*/

			if (r0 < R0_MAX) { /*Si el valor de r0  es menor que el valor maximo establecido, entra al if*/

				r0 = r0 + PASO_MS;
			}

			else
				r0 = r0;
	}
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();


	timer_config timer_adc={TIMER_A,2, FuncionTimerADC}; /* frec muestreo f=500 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&timer_adc);
	TimerStart(timer_adc.timer); /*o podria colocar directamente TIMER_A */


	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);
	AnalogOutputInit();


	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);






}


/*==================[end of file]============================================*/

