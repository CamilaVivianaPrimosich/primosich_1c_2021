/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Camila Primosich
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_4_ECG_PDIG.h"       /* <= own header */

#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

#define BAUDIO 115200  /*Transmitir los datos por la UART en formato ASCII a una velocidad de
						transmisión suficiente para realizar conversiones a la frecuencia requerida. */
#define BUFFER_SIZE 231
#define FREC_MAX 50
#define FREC_MIN 5
#define FREC_PASO 5
#define DT 0.002

const float PHI=3.14;
const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

bool bandera_adc= false;
bool bandera_dac= false;
uint16_t analog_value;
uint8_t i=0;
bool filtro=false;
float salida_filtrada=0;/* Variables anteriores  y actuales no un vector*/
float salida_filtrada_ant=0;
float entrada=0;
float fc=25; /*por defecto la frecuencia de corte del filtro es de 25 Hz*/
float rc=0;
float alfa=0;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void FuncionTimerADC(){
	/*Disparar la conversión AD (es decir que comience la conversion) a través de una interrupción periódica de timer.*/
	AnalogStartConvertion();
}

void FuncionAnalog(){
	AnalogInputRead(CH1,&analog_value); /*leemos un nuevo dato*/
	bandera_adc=true;/*Cambio de bandera de conversión AD */
}

void FuncionUart(){

}


void FuncionTimerDAC(){
	bandera_dac=true;  /*Bandera de conversión DA */
}


void Tecla1(){
	filtro=true;    /*Cuando presiono la tecla 1, la bandera de que el filtro esta activo pasa a ser verdadera*/
}
void Tecla2(){
	filtro=true;   /*Cuando presiono la tecla 2, la bandera del filtro pasa a estar desactivada es falsa*/
}

void Tecla3(){

	if (fc >= FREC_MIN) { /*Si la frecuencia de corte es mayor o igual que la de corte minima establecida, entra al if*/
		fc = fc - FREC_PASO;
		rc = 1 / ( 2 * PHI * fc );
		alfa = DT / ( DT + rc );
	}
	else {
		fc = fc;
	}
}

void Tecla4(){
	if (fc <= FREC_MAX) { /*Si la frecuencia de corte es menor o igual que la de corte minima establecida, entra al if*/
			fc = fc + FREC_PASO;
			rc = 1 / ( 2 * PHI * fc );
			alfa = DT / ( DT + rc );
		}
	else {
		fc = fc;
	}
}


void Filtro_PasaBajos (){

	entrada=analog_value;
	if (filtro == true){
		salida_filtrada=salida_filtrada_ant+alfa*(entrada-salida_filtrada_ant);
		salida_filtrada_ant=salida_filtrada;
	}
	else{
		salida_filtrada=entrada; /*si no se activo el filtro la salida filtrada=ECG*/
	}
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	SwitchesInit();/*Inicializo las teclas*/

	timer_config timer_adc={TIMER_A,2, FuncionTimerADC}; /* frec muestreo f=500 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&timer_adc);
	TimerStart(timer_adc.timer); /*o podria colocar directamente TIMER_A */


	timer_config timer_dac={TIMER_B,5, FuncionTimerDAC}; /* frec muestreo f=200 Hz, como el timer A, tiene el periodo en miliseg T=1/f=5 ms*/
	TimerInit(&timer_dac);
	TimerStart(timer_dac.timer); /*o podria colocar directamente TIMER_B */

	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);
	AnalogOutputInit();

	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);


	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);
	SwitchActivInt(SWITCH_4,Tecla4);

	rc = 1 / ( 2 * PHI * fc );
	alfa = DT / ( DT + rc );

    while(1){


    	if (bandera_dac == true){
    		if (i < BUFFER_SIZE){
    			AnalogOutputWrite(ecg[i]); /* va recorriendo el vector enviando los datos del ECG al conversor digital analogico*/
    			i++;
    		}
    		else{
    			i = 0;
    		}
    		bandera_dac = false;
    	}

    	if (bandera_adc == true){
    		UartSendString(SERIAL_PORT_PC, UartItoa(analog_value, 10));
    		UartSendString(SERIAL_PORT_PC,",");
    		/*Se deben enviar los datos convertidos separados por el caracter de fin de línea
    		* ("11,22,33\r" para el caso de 3 canales con los datos 11, 22 y 33 para
    		* el canal 1, 2 y 3 respectivamente)*/

    		Filtro_PasaBajos();

    		UartSendString(SERIAL_PORT_PC, UartItoa((uint16_t) salida_filtrada, 10));
    		UartSendString(SERIAL_PORT_PC,"\r");
    		bandera_adc=false;
    	}

    

    }
}

/*==================[end of file]============================================*/

