/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Proyecto_3.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUDIO 115200
bool encendido= false;
bool hold= false;
uint16_t distancia=0;
bool bandera_timer= false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void Tecla1(){
	if(encendido == false){
	   encendido = true;
	   /*TimerStart(my_timer.timer);*/ /*si lo quiero hacer asi, tengo que definirla como global*/}
	else{
		encendido = false;
		/*TimerStop(my_timer.timer);*/}}

void Tecla2(){
	if(hold == false)
	hold = true;
	else hold = false;
}

void FuncionTimer(){
	bandera_timer=true;

	UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));
	UartSendString(SERIAL_PORT_PC," ");
	UartSendString(SERIAL_PORT_PC,"cm");
	UartSendString(SERIAL_PORT_PC,"\r\n");
}


void EncenderLedsyMostrarLCD(){

	if (bandera_timer == true){
		if(encendido == true){
		    distancia= HcSr04ReadDistanceCentimeters();
		    if(hold == false){
		    	if( (distancia>=2) && (distancia<=10)){
		    		LedOn(LED_RGB_B);
		    		LedOff(LED_RGB_G);
		    		LedOff(LED_RGB_R);
		    		LedOff(LED_1);
		    		LedOff(LED_2);
		    		LedOff(LED_3);}
		    	if((distancia>10) && (distancia<=20)){
		    		LedOn(LED_RGB_B);
		    		LedOff(LED_RGB_G);
		    		LedOff(LED_RGB_R);
		    		LedOn(LED_1);
		    		LedOff(LED_2);
		    		LedOff(LED_3);}
		    	if((distancia>20) && (distancia<=30)){
		    		LedOn(LED_RGB_B);
		    		LedOff(LED_RGB_G);
		    		LedOff(LED_RGB_R);
		    		LedOn(LED_1);
		    		LedOn(LED_2);
		    		LedOff(LED_3);}
		    	if((distancia>30) && (distancia<130)){
		    		LedOn(LED_RGB_B);
		    		LedOff(LED_RGB_G);
		    		LedOff(LED_RGB_R);
		    		LedOn(LED_1);
		    		LedOn(LED_2);
		    		LedOn(LED_3);}

		    	ITSE0803DisplayValue(distancia);}
			}

		else {
			LedsOffAll();
			distancia=0;
			ITSE0803DisplayValue(distancia);}

	bandera_timer= false;}
}


void FuncionUart(){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC,&dato);
		if ((dato== 'O') || (dato=='o')){ /*leo el valor que tengo en el puerto, si es O u o, entra a la función Tecla1*/
			Tecla1();}
		if ((dato == 'H') || (dato == 'h')){ /*leo el valor que tengo en el puerto, si es H u h, entra a la función Tecla2*/
			Tecla2();}
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	LedsInit();

	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	gpio_t pin[7]= { GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5 };
	ITSE0803Init(pin);

	/*uint8_t tecla_actual, tecla_anterior;*/
	SwitchesInit();
	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);

	timer_config my_timer={TIMER_A,1000, FuncionTimer};
	TimerInit(&my_timer);

	TimerStart(my_timer.timer);

	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);


    while(1){

    	EncenderLedsyMostrarLCD();

    }

}


/*==================[end of file]============================================*/

