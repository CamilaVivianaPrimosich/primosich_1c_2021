var indexSectionsWithContent =
{
  0: "abdefmprstv",
  1: "p",
  2: "efmrt",
  3: "abefprsv",
  4: "bdf",
  5: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros",
  5: "Pages"
};

