/*! @mainpage Proyecto 5: Adquision, procesamiento y visualizacion por Osciloscopio de una señal de EMG
 *
 *
 * \section genDesc General Description
 *
 * La aplicación permite el control de una protesis de mano, a traves del sensado de la señal de EMG del antebrazo, permite detectar si el puño se encuentra abierto, cerrado
 * y si se esta realizando una contraccion fuerte del mismo
 *
 * \section hardConn Hardware Connection
 *
 * | PLACA ADQ. EMG |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   J1 canal 2 	| 	   CH1		|
 * |   	  VCC	 	| 	  +5V		|
 * |   	  GND	 	| 	   GND		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Primosich Camila Viviana
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_5_EMG.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "led.h"

/*==================[macros and definitions]=================================*/
/** @def Baudio
 * @brief Transmitir los datos de la señal por segundo a traves de la UART a una velocidad de transmisión suficiente.
 */
#define BAUDIO 115200

/** @def FREC_MAX
 * @brief Valor de la frecuencia de corte maxima permitida por el sistema
 */
#define FREC_MAX 5

/** @def FREC_MIN
 * @brief Valor de la frecuencia de corte minima permitida por el sistema
 */
#define FREC_MIN 0.5

/** @def FREC_PASO
 * @brief Valor de la frecuencia que se le suma o se le resta a la frecuencia de corte, dependiendo la opcion elegida
 */
#define FREC_PASO 0.5

/** @def DT
 * @brief Valor de la constante para calcular el valor de alfa en el filtro
 */
#define DT 0.002

/** @def PI
 * @brief Definición del valor de la constante pi
 */
const float PHI = 3.14;

bool bandera_adc = false; /* Bandera para la lectura del valor analogico de entrada */
bool procesamiento = false;  /* Bandera que se utiliza para determinar si se requiere hacer el procesamiento de la señal o no, dependiendo si se activo la misma con la tecla 1 */

uint16_t analog_value;

float entrada = 0; /* Variable de entrada*/
float salida_filtrada = 0; /* Variable de salida*/
float salida_filtrada_ant = 0; /* Variable anterior de la salida*/


float salida_sin_continua = 0;
float salida_sin_continua_anterior = 0;
float rectificacion = 0;

/** @def fc
 * @brief frecuencia de corte del filtro pasa bajos, que tiene por defecto el sistema (3 Hz)
 */
float fc = 3;
float rc = 0; /* parametro alfa que se usa para el filtro pasabajos */
float alfa = 0; /* parametro alfa que se usa para el filtro pasabajos */
float VC = 430; /* Valor de voltaje continuo que se le resta a la señal */

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


/**@fn void FuncionTimerADC()
 * @brief  Función que disparar la conversión A-D (es decir que comience la conversion) a través de una interrupción periódica de timer.
 * @return None
 */
void FuncionTimerADC() {
	AnalogStartConvertion();
}

/**@fn void FuncionAnalog()
 * @brief  Función que lee un nuevo dato analogico, proveniente del canal CH1 de la EDU-CIAA
 * @return None
 */
void FuncionAnalog() {
	AnalogInputRead(CH1, &analog_value); /*Se lee un nuevo dato*/
	bandera_adc = true;/*Cambio de bandera de conversión AD */
}
/**@fn void FuncionUart()
 * @brief  Funcion que esta definida para la interrupcion de la UART, pero en esta versión no se encuentra implementada
 * @return None
 */
void FuncionUart() {
}

/**@fn void EliminarContinua()
 * @brief  Función que elimina el valor de continua que posee el dato analogico a su entrada
 * @return None
 */
void EliminarContinua() {

	entrada = analog_value;

	if (procesamiento == true) {
		salida_sin_continua = entrada - VC;
	}

	else {
		salida_sin_continua = entrada; /*si no se activo, la salida va a estar sin eliminar el valor de continua */
	}
}

/**@fn void Rectificar()
 * @brief  Función que rectifica la señal, es decir, si la señal toma un valor negativo, la vuelve positiva
 * @return None
 */
void Rectificar() {

	if (salida_sin_continua < 0) {
		rectificacion = -salida_sin_continua;
	} else {
		rectificacion = salida_sin_continua; /*si no se activo, la salida va a estar sin eliminar el valor de continua */
	}
}

/**@fn void FiltroPasaBajos()
 * @brief  Función que aplica un filtro pasabajos, recibiendo el valor de la señal original y el valor anterior filtrado
 * @return None
 */
void FiltroPasaBajos() {

	entrada = rectificacion;

	if (procesamiento == true) {
		salida_filtrada = salida_filtrada_ant + alfa * (entrada - salida_filtrada_ant);
		salida_filtrada_ant = salida_filtrada;
	} else {
		salida_filtrada = entrada; /*si no se activo el filtro la salida filtrada=EMG de entrada*/
	}

}

/**@fn void Tecla1()
 * @brief  Función que controla la tecla 1 de la placa. Activa el procesamiento de la señal, es decir, la correccion de offset, rectificacion y filtro pasabajos
 * @return None
 */
void Tecla1() {
	procesamiento = true; /*Cuando presiono la tecla 1, la bandera de que el filtro esta activo pasa a ser verdadera*/
}

/**@fn void Tecla2()
 * @brief  Función que controla la tecla 2 de la placa. Desactiva el procesamiento de la señal
 * @return None
 */
void Tecla2() {
	procesamiento = false; /*Cuando presiono la tecla 2, la bandera del filtro pasa a estar desactivada es falsa*/
}

/**@fn void Tecla3()
 * @brief  Función que controla la tecla 3 de la placa. Disminuye la frecuencia de corte del filtro pasabajos en 0.5 Hz
 * @return None
 */
void Tecla3() {

	if (procesamiento == true) {
		if (fc >= FREC_MIN) { /*Si la frecuencia de corte es mayor o igual que la de corte minima establecida, entra al if*/
			fc = fc - FREC_PASO;
			rc = 1 / (2 * PHI * fc);
			alfa = DT / ( DT + rc);
		} else {
			fc = fc;
		}
	}
}
/**@fn void Tecla4()
 * @brief  Función que controla la tecla 4 de la placa. Aumenta la frecuencia de corte del filtro pasabajos en 0.5 Hz
 * @return None
 */
void Tecla4() {

	if (procesamiento == true) {
		if (fc <= FREC_MAX) { /*Si la frecuencia de corte es menor o igual que la de corte maxima establecida, entra al if*/
			fc = fc + FREC_PASO;
			rc = 1 / (2 * PHI * fc);
			alfa = DT / ( DT + rc);
		} else {
			fc = fc;
		}
	}
}

/**@fn void EncenderLedsyEnviarDatosUart()
 * @brief  Función que  detecta si la mano se encuentra abierta, con puño cerrado y con puño cerrado con contraccion fuerte de los muesculos del antebrazo
 * Dependiendo del valor que tome, indicara por la salida del puerto serie que es lo que se esta realizando y se podra visualizar a traves de los leds con el siguiente representacion:
 * LED BLUE: La mano se encuentra abierta, sin contraccion del antebrazo y de la mano
 * LED GREEN: el puño se encuentra cerrado, con contraccion leve
 * LED red: el puño se encuentra cerrado, con contraccion fuerte
 * @return None
 */
void EncenderLedsyEnviarDatosUart() {

	if (procesamiento == true) {
		if ((salida_filtrada >= 0) && (salida_filtrada <= 40)) {
			LedOn(LED_RGB_B);
			LedOff(LED_RGB_G);
			LedOff(LED_RGB_R);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			//UartSendString(SERIAL_PORT_PC,"La mano se encuentra abierta, no hay contraccion \r\n");
		}
		if ((salida_filtrada > 40) && (salida_filtrada <= 95)) {
			LedOff(LED_RGB_B);
			LedOn(LED_RGB_G);
			LedOff(LED_RGB_R);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			//UartSendString(SERIAL_PORT_PC,"El puño se encuentra cerrado, existe una contraccion\r\n");
		}
		if (salida_filtrada > 95) {
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);
			LedOn(LED_RGB_R);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			//UartSendString(SERIAL_PORT_PC,"El puño se encuentra cerrado con contraccion fuerte\r\n");
		}
	}

	else {
		LedsOffAll();
	}
}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	SystemClockInit();
	SwitchesInit(); /*Inicializo las teclas*/
	LedsInit(); /*Inicializo los leds*/

	timer_config timer_adc = { TIMER_A, 2, FuncionTimerADC }; /* frec muestreo f=500 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&timer_adc);
	TimerStart(timer_adc.timer); /*o podria colocar directamente TIMER_A */

	analog_input_config my_analog = { CH1, AINPUTS_SINGLE_READ, FuncionAnalog };
	AnalogInputInit(&my_analog);
	AnalogOutputInit();

	serial_config my_uart = { SERIAL_PORT_PC, BAUDIO, FuncionUart };
	UartInit(&my_uart);

	/* Manejo de teclas de la placa por interrupciones */
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

	/*Calculo de parametros del filtro pasabajos*/
	rc = 1 / (2 * PHI * fc); /* Calculo rc*/
	alfa = DT / ( DT + rc); /* Calculo de alfa*/

	while (1) {

		if (bandera_adc == true) {
			UartSendString(SERIAL_PORT_PC, UartItoa(analog_value, 10));
			UartSendString(SERIAL_PORT_PC, ",");

			EliminarContinua();
			Rectificar();
			FiltroPasaBajos();

			UartSendString(SERIAL_PORT_PC, UartItoa((uint16_t) salida_filtrada, 10));
			UartSendString(SERIAL_PORT_PC, "\r");

			EncenderLedsyEnviarDatosUart();
			bandera_adc = false;
		}

	}
}

/*==================[end of file]============================================*/

