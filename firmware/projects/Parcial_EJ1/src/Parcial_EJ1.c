/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 9/06/2021 | Document creation		                         |
 * |			|							                     |
 *
 *
 *
 * Consigna: Diseñar e implementar el firmware de un odómetro (medidor de distancia).
 * El mismo está implementado usando un sensor TCRT5000 que debe contar la cantidad de pulsos generados por
 * un disco perforado solidario a una rueda.
 * El disco posee 20 aberturas y la rueda 32cm de diámetro. En funcionamiento,
 * el sistema debe ir acumulando la distancia recorrida, e informarla a través de la UART cada un segundo,
 * con el formato “XX cm\r\n”.  Para el control del sistema se deben usar las teclas:
	Tecla 1: Encendido.
	Tecla 2: Apagado.
	Tecla 3: Reseteo de cuenta
 *
 *
 * @author Camila Primosich
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial_EJ1.h"
#include "systemclock.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "Tcrt5000.h"

/*==================[macros and definitions]=================================*/
#define BAUDIO 115200
#define OBJETO 1
#define NO_OBJETO 0
#define RADIO 16
#define NUM_ABERTURAS 20

const float PHI=3.14;

bool encendido= false;
bool reseteo=false;
uint16_t distancia=0;
bool bandera_timer= false;
bool estado= false;
bool estadoant = false;
uint16_t contador=0;
uint16_t distancia_vuelta = 0;
uint32_t distancia_recorrida = 0;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void Tecla1(){
	   encendido = true;
}



void Tecla2(){
	encendido = false;
}

void Tecla3(){

	contador = 0;
	distancia_recorrida = 0;
	reseteo=false;
}

void FuncionTimer(){
	bandera_timer=true;

	UartSendString(SERIAL_PORT_PC, UartItoa(distancia_recorrida , 10));
	UartSendString(SERIAL_PORT_PC," ");
	UartSendString(SERIAL_PORT_PC,"cm");
	UartSendString(SERIAL_PORT_PC,"\r\n");
}



void FuncionUart(){

}



void Odometro(){

	if (bandera_timer == true){

		if(encendido == true){

			estado = Tcrt5000State();/*Leo y guardo el estado del puerto del Tcrt5000*/

			if ((estado == OBJETO) && (estadoant == NO_OBJETO)){

				contador = contador + 1; /*Si el estado es 1, detecta una ranura y lo suma al contador */
			}
		}

	distancia_vuelta = 2 * PI * RADIO;

	distancia_recorrida = ( contador * distancia_vuelta ) / NUM_ABERTURAS;

	bandera_timer= false;

	estadoant = estado;/*Le coloca a la variable estado anteror, el valor actual que lee el puerto*/
	}
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();

	/*Inicializo el  Tcrt5000*/
	Tcrt5000Init(GPIO_T_COL0);

	/*Teclas*/
	SwitchesInit();
	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);

	/*Timer*/
	timer_config my_timer={TIMER_A,1000, FuncionTimer}; /*Debe informar la distancia cada un segundo, por lo tanto como el Timer A tiene el periodo en ms es 1000ms*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);

	/*Comunicacion serie*/
	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);


    while(1){

    	Odometro();

    }

}


/*==================[end of file]============================================*/

