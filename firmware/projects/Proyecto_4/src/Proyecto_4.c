/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Camila Primosich
 *
 */

/*==================[inclusions]=============================================*/
#include "Proyecto_4.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

#define BAUDIO 115200  /*Transmitir los datos por la UART en formato ASCII a una velocidad de
						transmisión suficiente para realizar conversiones a la frecuencia requerida. */
bool bandera_timer= false;
uint16_t analog_value;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void FuncionTimer(){
	/*Disparar la conversión AD (es decir que comience la conversion) a través de una interrupción periódica de timer.*/
	AnalogStartConvertion();
}

void FuncionAnalog(){
	AnalogInputRead(CH1,&analog_value);
	UartSendString(SERIAL_PORT_PC, UartItoa(analog_value, 10));
	UartSendString(SERIAL_PORT_PC,"\r");
	/*Se deben enviar los datos convertidos separados por el caracter de fin de línea
	* ("11,22,33\r" para el caso de 3 canales con los datos 11, 22 y 33 para
	* el canal 1, 2 y 3 respectivamente)*/
}

void FuncionUart(){

}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();

	timer_config my_timer={TIMER_A,2, FuncionTimer}; /* frec muestreo f=500 Hz, como el timer A, tiene el periodo en miliseg T=1/f=2 ms*/
	TimerInit(&my_timer);

	TimerStart(my_timer.timer);

	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);

	serial_config my_uart ={SERIAL_PORT_PC,BAUDIO, FuncionUart};
	UartInit(&my_uart);

    while(1){

	}
    

}

/*==================[end of file]============================================*/

