/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Ejercicio1_G2_Interrupciones.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

bool encendido= false;
bool hold= false;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void Tecla1(){
	if(encendido == false)
	   encendido = true;
	else encendido = false;
}
void Tecla2(){
	if(hold == false)
	hold = true;
	else hold = false;
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	uint16_t distancia=0;

	SystemClockInit();
	LedsInit();

	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	gpio_t pin[7]= { GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5 };
	ITSE0803Init(pin);

	/*uint8_t tecla_actual, tecla_anterior;*/
	SwitchesInit();
	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);


    while(1){

    /*tecla_actual  = SwitchesRead();
    tecla_anterior=tecla_actual; /*coloco el valor anterior de la tecla, el valor actual que tiene*/

    if(encendido == true){
    	distancia= HcSr04ReadDistanceCentimeters(); /*Leo y guardo el valor de distancia en centimetros del puerto*/
    		if(hold == false){
    			if( (distancia>=0) && (distancia<=10)){
    	    		LedOn(LED_RGB_B);
    	    		LedOff(LED_RGB_G);
    	    		LedOff(LED_RGB_R);
    	    		LedOff(LED_1);
    	    		LedOff(LED_2);
    	    		LedOff(LED_3);}
    	    	if((distancia>10) && (distancia<=20)){
    	    	    LedOn(LED_RGB_B);
    	    	    LedOff(LED_RGB_G);
    	    	    LedOff(LED_RGB_R);
    	    	    LedOn(LED_1);
    	    	    LedOff(LED_2);
    	    	    LedOff(LED_3);}
    	    	if((distancia>20) && (distancia<=30)){
    	    	    LedOn(LED_RGB_B);
    	    	    LedOff(LED_RGB_G);
    	    	    LedOff(LED_RGB_R);
    	    	    LedOn(LED_1);
    	    	    LedOn(LED_2);
    	    	    LedOff(LED_3);}
    	    	if(distancia>30){
    	    	    LedOn(LED_RGB_B);
    	    	    LedOff(LED_RGB_G);
    	    	    LedOff(LED_RGB_R);
    	    	    LedOn(LED_1);
    	    	    LedOn(LED_2);
    	    	    LedOn(LED_3);}

    	    ITSE0803DisplayValue(distancia);}
    		}
    	else {
			LedOff(LED_RGB_R);
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			distancia=0;
		}
    }
}


/*==================[end of file]============================================*/

