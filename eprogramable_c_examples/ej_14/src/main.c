/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* Ejercicio 14:
 * Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
A.Defina una variable con esa estructura y cargue los campos con sus propios datos.
B.Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
 *
 *  */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
/*==================[macros and definitions]=================================*/

 /* struct alumno {
		char Nombre [12];
		char Apellido [20];
		uint8_t edad;
	} alumno_1, alumno_2;

struct alumno alumno_3; /* es otra forma de crear una variable de la estructura alumno*/


/* Otra forma */

typedef struct {
		char nombre [12];
		char apellido [20];
		uint8_t edad;
	} alumno;

/*==================[internal functions declaration]=========================*/

int main(void)
{


	/*alumno alumna_1= {"Camila","Primosich", 25};*/

	alumno alumna_1;
	alumna_1.edad=25;
	strcpy (alumna_1.nombre, "camila");
	strcpy (alumna_1.apellido, "primosich");


	printf ("Edad alumna 1: %d\r\n", alumna_1.edad);
	printf ("Nombre alumna 1: %s\r\n", alumna_1.nombre);
	printf ("Apellido alumna 1: %s\r\n", alumna_1.apellido);



	alumno alumna_2;
	alumno *alumna_2ptr= &alumna_2;

	strcpy (alumna_2ptr->nombre, "Ana");
	strcpy (alumna_2ptr->apellido, "Roskopf");
	alumna_2ptr->edad=25;

	printf("%s\r\n",alumna_2ptr->nombre);
	printf("%s\r\n",alumna_2ptr->apellido);
	printf("%d\r\n",alumna_2ptr->edad);

	return 0;
}

/*==================[end of file]============================================*/

