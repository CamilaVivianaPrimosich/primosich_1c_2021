########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = ej_1
#NOMBRE_EJECUTABLE = ejercicio1.exe

#PROYECTO_ACTIVO = ej_2
#NOMBRE_EJECUTABLE = ejercicio2.exe

#PROYECTO_ACTIVO = ej_3
#NOMBRE_EJECUTABLE = ejercicio3.exe

#PROYECTO_ACTIVO = ej_4
#NOMBRE_EJECUTABLE = ejercicio4.exe

#PROYECTO_ACTIVO = ej_5
#NOMBRE_EJECUTABLE = ejercicio5.exe

#PROYECTO_ACTIVO = ej_7
#NOMBRE_EJECUTABLE = ejercicio7.exe

#PROYECTO_ACTIVO = ej_9
#NOMBRE_EJECUTABLE = ejercicio9.exe

#PROYECTO_ACTIVO = ej_12
#NOMBRE_EJECUTABLE = ejercicio12.exe

#PROYECTO_ACTIVO = ej_14
#NOMBRE_EJECUTABLE = ejercicio14.exe

#PROYECTO_ACTIVO = ej_16
#NOMBRE_EJECUTABLE = ejercicio16.exe

#PROYECTO_ACTIVO = ej_17
#NOMBRE_EJECUTABLE = ejercicio17.exe

#PROYECTO_ACTIVO = ej_A
#NOMBRE_EJECUTABLE = ejercicioA.exe

PROYECTO_ACTIVO = ej_A2
NOMBRE_EJECUTABLE = ejercicioA2.exe

#PROYECTO_ACTIVO = ej_C
#NOMBRE_EJECUTABLE = ejercicioC.exe

#PROYECTO_ACTIVO = ej_D
#NOMBRE_EJECUTABLE = ejercicioD.exe